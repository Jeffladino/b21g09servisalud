const express = require('express');
const Usuario = require('../models/usuario_models');
const ruta = express.Router();
const Joi = require('joi');
const bcrypt = require('bcrypt');

const schema = Joi.object({
    nombre: Joi.string()
        .min(3)
        .max(30)
        .required(),
    
    apellido: Joi.string()
        .min(3)
        .max(30)
        .required(),
    
    cedula: Joi.number()
        .integer()
        .required(),
    
    telefono: Joi.number()
        .integer()
        .required(),
    
    direccion: Joi.string()
        .min(3)
        .max(30)
        .required(),

    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),


    email: Joi.string()
        .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
});

ruta.get('/',(req, res)=>{
    let resultado = listarUsuariosActivos();
    resultado.then(users =>{
        res.json({
            valor: users
        })
    }).catch(err =>{
        res.status(400).json({
            err
        })
    });
 });

 ruta.post('/', (req, res)=>{
    let body = req.body;

    Usuario.findOne({email:body.email}, (err, user)=> {
        if(err){
            return res.status(400).json({error:'server error'});
        } 
        if(user){
            //Si el usuario existe
            return res.status(400).json({msg: 'El usuario ya existe'});
        }
    });

    const {error, value} = schema.validate({nombre:body.nombre, apellido:body.apellido,  cedula:body.cedula, 
        telefono:body.telefono, direccion:body.direccion,email:body.email, password:body.password});

    if(!error){
        let resultado = crearUsuario(body);

        resultado.then(user =>{
            res.json({
                nombre:user.nombre,
                apellido: user.apellido,

                email:user.email
            })
        }).catch(err => {
            res.status(400).json({
                error: err
            })
        });
    }else{
        res.status(400).json({
            error
        })
    }

});


ruta.put('/:email', (req, res)=>{
    let body = req.body;
    const {error, value} = schema.validate({nombre:body.nombre, apellido:body.apellido,  cedula:body.cedula, 
        telefono:body.telefono, direccion:body.direccion,email:body.email, password:body.password});

        if(!error){
            let resultado = actualizarUsuario(req.params.email, req.body);
            resultado.then(valor =>{
                res.json(valor)
            }).catch(err => {
                res.status(400).json(err);
            });
    
        }else {
            res.status(400).json({
                error
            });
        }
 });

 ruta.delete('/:email',(req, res)=>{
    let resultado = cambiarEstado(req.params.email);
    resultado.then(valor =>{
        res.json({
           nombre: valor.nombre,
           apellido: valor.apellido,
           cedula: valor.cedula,
           email: valor.email,
           msj: 'usuario eliminado'
        })
    }).catch(err =>{
        res.status(400).json({
            error: err
        });
    });
})

 
async function listarUsuariosActivos(){
    let usuarios = await Usuario.find({'estado':true})
    .select({nombre:1, apellido:1, cedula:1, direccion:1, telefono:1, email:1, password:1})
    //.populate('perfil', 'nombre_perfil -_id');
    return usuarios;
}

async function crearUsuario(body){
    let usuario = new Usuario({
        nombre      :body.nombre,
        apellido    :body.apellido,
        cedula      :body.cedula,
        telefono    :body.telefono,
        direccion   :body.direccion,
        email       :body.email,
        password    : bcrypt.hashSync(body.password, 10)
    });

    return await usuario.save();
}

async function actualizarUsuario(email, body){
    let usuario = await Usuario.findOneAndUpdate({"email": email}, {
        $set: {
            nombre: body.nombre,
            apellido:body.apellido,
            cedula: body.cedula,
            telefono: body.cedula,
            direccion: body.direccion,
            email: body.email,
            password: bcrypt.hashSync(body.password, 10)
        }
    }, {new:true});


    return usuario
}

async function cambiarEstado(email){
    let usuario = await Usuario.findOneAndUpdate({"email": email}, {
        $set: {
            estado: false
        }
    }, {new:true});


    return usuario
}

module.exports = ruta;


