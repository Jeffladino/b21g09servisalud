const express = require('express');
const mongoose = require('mongoose');
const usuarios = require('./routes/usuarios');

//conectandose a la BD
mongoose.connect("mongodb://localhost:27017/servisalud", {useUnifiedTopology: true, useNewUrlParser: true})
    .then(()=>console.log('conectado a mongoDB.........'))
    .catch(err=>console.log('No se pudo conectar con MongoDB ', err))
mongoose.set('useCreateIndex', true);


// llamando al servicio
const app = express();
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use('/api/usuarios', usuarios);

//iniciando el servicio

const port =  process.env.PORT || 3000;
app.listen(port, () =>{
    console.log("API RESTFUL OK, Y EJECUTANDOSE......");
} );