const mongoose = require('mongoose');

const usuarioEsquema = new mongoose.Schema({
    email:{
        type:String,
        require:true,
        unique:true
    },
    nombre:{
        type:String,
        require:true
    },
    apellido:{
        type:String,
        require:true
    },
    cedula:{
        type:String,
        require:true
    },
    direccion:{
        type:String,
        require:true
    },
    telefono:{
        type:String,
        require:true
    },
    password:{
        type:String,
        require:true
    },
    estado:{
        type:Boolean,
        default:true
    }

});

module.exports = mongoose.model('usuario', usuarioEsquema);